/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sonarqubefeatures.calc;

import static org.junit.Assert.*;
import org.junit.Test;


/**
 *
 * @author marlo
 */
public class SimpleCalculatorTest {
    
    
    @Test
    public void testIntegerSum(){
        SimpleCalculator sc = new SimpleCalculator();
        final double VALUE = 10;
        double result = sc.sum(VALUE);
        
        assertEquals(result, VALUE, 0.0001d);
    }
    
    @Test
    public void testIntegerSubtract(){
        final double INITIAL=100;
        SimpleCalculator sc = new SimpleCalculator(INITIAL);
        final double VALUE = 10;
        double result = sc.subtract(VALUE);
        
        assertEquals(result, (INITIAL-VALUE), 0.0001d);   
    }
    
    @Test
    public void testIntegerMultiply(){
        final double INITIAL=100;
        SimpleCalculator sc = new SimpleCalculator(INITIAL);
        final double VALUE = 10;
        double result = sc.multiply(VALUE);
        
        assertEquals(result, (INITIAL*VALUE), 0.0001d);   
    }
}
