

    import java.util.List;
     
    import org.aspectj.lang.reflect.CodeSignature;
     
    import com.myapp.util.AspectUtils;;
     
    public aspect ServicesLogger {
     
            pointcut logMethod():
                    //execution(!@com.myapp.util.NO_LOG *  com.myapp.services..* (..));
                execution(!@com.myapp.util.NO_LOG *  com.myapp.services..* (..));
     
            before(): logMethod() {
                System.out.printf("Enters on method: %s. \n", thisJoinPoint.getSignature());
                    Object[] paramValues = thisJoinPoint.getArgs();
                    String[] paramNames = ((CodeSignature) thisJoinPointStaticPart
                                    .getSignature()).getParameterNames();
                    StringBuilder logLine = new StringBuilder(thisJoinPointStaticPart
                                    .getSignature().getName()).append("(");
                    if (paramNames.length != 0)
                            AspectUtils.logParamValues(logLine, paramNames, paramValues);
                    logLine.append(") - started");
                    AspectUtils.getLogger(thisJoinPoint).info(logLine.toString());
            }
     
            @SuppressWarnings("rawtypes")
            after() returning(Object r): logMethod(){
     
                    if (r != null && (!(r instanceof List) || ((List) r).size() != 0)) {
                            StringBuilder rv = new StringBuilder("Return Value : ");
                            rv.append(AspectUtils.toString(r));
                            AspectUtils.getLogger(thisJoinPoint).info(rv.toString());
                    }
     
                    Object[] paramValues = thisJoinPoint.getArgs();
                    String[] paramNames = ((CodeSignature) thisJoinPointStaticPart
                                    .getSignature()).getParameterNames();
                    StringBuilder logLine = new StringBuilder(thisJoinPointStaticPart
                                    .getSignature().getName()).append("(");
                    if (paramNames.length != 0)
                            AspectUtils.logParamValues(logLine, paramNames, paramValues);
                    logLine.append(") - finished");
                    AspectUtils.getLogger(thisJoinPoint).info(logLine.toString());
            }
    }

