/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.services;

import sonarqubefeatures.calc.SimpleCalculator;

/**
 *
 * @author marlo
 */
public class Principal {
    
    public static void main(String... args){
        final double INITIAL=100;
        testIntegerMultiply();
        testIntegerSum();
        testIntegerSubtract();
        System.out.println("teste...");
    }
    
    public static void testIntegerSum(){
        SimpleCalculator sc = new SimpleCalculator();
        final double VALUE = 10;
        double result = sc.sum(VALUE);
    }
    
    public static void testIntegerSubtract(){
        final double INITIAL=100;
        SimpleCalculator sc = new SimpleCalculator(INITIAL);
        final double VALUE = 10;
        double result = sc.subtract(VALUE);    
    }
    
    public static void testIntegerMultiply(){
        final double INITIAL=100;
        SimpleCalculator sc = new SimpleCalculator(INITIAL);
        final double VALUE = 10;
        double result = sc.multiply(VALUE);   
    }
    
}
