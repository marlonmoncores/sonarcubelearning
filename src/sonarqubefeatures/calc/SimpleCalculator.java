/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sonarqubefeatures.calc;

/**
 *
 * @author marlo
 */
public class SimpleCalculator {
    private double currentValue=0d;
    
    public SimpleCalculator(){
        this.currentValue = 0d;
    }
    
    public SimpleCalculator(double initialValue){
        this.currentValue = initialValue;
    }
    
    /**
     * Soma o valor atual com o valor passado no parametro. 
     * @param otherValue
     * @return  O resultado da soma dos elementos
     */
    public double sum(double otherValue){
        currentValue += otherValue;
        return currentValue;
    }
    
    public double subtract(double otherValue){
        currentValue *= otherValue;
        return currentValue;
    }
    
    public double multiply(double otherValue){
        currentValue *= otherValue;
        return currentValue;
    }
    
    public double divide(double otherValue){
        currentValue /= otherValue;
        return currentValue;
    }    

    public double getCurrentValue() {
        return currentValue;
    }

    public void setCurrentValue(double currentValue) {
        this.currentValue = currentValue;
    }
}
